@echo off
echo "Delete repos (1)"
echo "Download & build repos (in parallel)(2)"
echo "Download & build repos (3)"
set /p "in=--> "
set /p "confirm=Continue? (Y/N): "

if /i "%confirm%" neq "Y" if /i "%confirm%" neq "Yes" exit /b 1

if "%in%" equ "1" (
    rmdir /s /q .\yamcs-instance
    rmdir /s /q .\frontend-software
)

if "%in%" equ "2" (
    if exist .\yamcs-instance (
        echo Yamcs exist
        start "" docker build -t yamcs-instance ./yamcs-instance
    ) else (
        echo Yamcs don't exist. Cloning...
        start "" git clone https://gitlab.com/acubesat/ops/yamcs-instance & docker build -t yamcs-instance ./yamcs-instance
    )

    if exist .\frontend-software (
        echo Frontend exist
        start "" docker build -t frontend-software ./frontend-software
    ) else (
        echo Frontend don't exist. Cloning...
        start "" git clone https://gitlab.com/acubesat/ops/frontend-software & docker build -t frontend-software ./frontend-software
    )
)

if "%in%" equ "3" (
    if exist .\yamcs-instance (
        echo Yamcs exist
        docker build -t yamcs-instance ./yamcs-instance
    ) else (
        echo Yamcs don't exist. Cloning...
        git clone https://gitlab.com/acubesat/ops/yamcs-instance
        docker build -t yamcs-instance ./yamcs-instance
    )

    if exist .\frontend-software (
        echo Frontend exist
        docker build -t frontend-software ./frontend-software
    ) else (
        echo Frontend don't exist. Cloning...
        git clone https://gitlab.com/acubesat/ops/frontend-software
        docker build -t frontend-software ./frontend-software
    )
)
