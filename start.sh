#!/bin/bash

echo "Delete repos (1)"
echo "Download & build repos (in parallel)(2)"
echo "Download & build repos (3)"
read -p "--> " in
read -p "Continue? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
if [ $in -eq "1" ]
then
	rm -rf ./yamcs-instance
	rm -rf ./frontend-software
fi

if [ $in -eq "2" ]
then
	if test -d ./yamcs-instance; then
		echo "Yamcs exist"
		docker build -t yamcs-instance ./yamcs-instance &
	else
		echo  "Yamcs don't exist. Clonning..."
		git clone https://gitlab.com/acubesat/ops/yamcs-instance; docker build -t yamcs-instance ./yamcs-instance &
	fi


	if test -d ./frontend-software; then
		echo "Frontend exist"
		docker build -t frontend-software ./frontend-software &
	else
		echo  "Frontend don't exist. Clonning..."
		git clone https://gitlab.com/acubesat/ops/frontend-software; docker build -t frontend-software ./frontend-software &
	fi

fi

if [ $in -eq "3" ]
then
	if test -d ./yamcs-instance; then
		echo "Yamcs exist"
		docker build -t yamcs-instance ./yamcs-instance
	else
		echo  "Yamcs don't exist. Clonning..."
		git clone https://gitlab.com/acubesat/ops/yamcs-instance
		docker build -t yamcs-instance ./yamcs-instance
	fi


	if test -d ./frontend-software; then
		echo "Frontend exist"
		docker build -t frontend-software ./frontend-software
	else
		echo  "Frontend don't exist. Clonning..."
		git clone https://gitlab.com/acubesat/ops/frontend-software
		docker build -t frontend-software ./frontend-software
	fi

fi



