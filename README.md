## Build & Run with Docker 
This will start the backend (based in YAMCS) and the frontend software.
The apps will be accessible in [localhost:8090](localhost:8090) for YAMCS and [localhost:80](localhost:80) for the frontend.

### Required Packages
- Docker (Linux/Windows)
- Docker-compose (Linux only)

### Linux & Mac
- Install the necessary software (In debian based systems run `apt install -y docker docker-compose` )
- Set permisions to `start.sh` with the command `chmod +x ./start.sh`
- Run the `start.sh` file.
- Select the **Second** or **Third** option (**Third oprion is recommended for low end PC**).
- Wait until the script is done.
- Run the software by executing the command `docker-compose up`. Add the option `-d` to run the command in the background


### Windows
- Install the Docker Desktop from [here](https://www.docker.com/products/docker-desktop/)
- Run the `start.bat` file.
- Select the **Second** or **Third** option (**Third oprion is recommended for low end PC**).
- Wait until the script is done.
- Start the Docker Engine if it isn't running by openning the program `Docker Desktop`
- Run the software by executing the command `docker compose up`. Add the option `-d` to run the command in the background

### General
- Delete the local files with the First option in the script
- To delete the local generated docker images execute `docker image/images ls ` to see all the images and then run `docker image/images rm <Image ID>` to delete the image with the specified ID

## Simulation & Board testing
This will be used to start the simulator and test the backend and frontend code OR test the software with the real boards connected via usb.

### Simulation
- Attach to container's terminal via `docker attach <container name>`
- Go to `/yamcs-instance`
- Start the simulator executing the command `python3 ./simulator.py`

### Board testing
- Attach to container's terminal via `docker attach <container name>`
- Go to `/yamcs-instance/communication`
- Start the handler by executing the command `python3 ./<handlerFile>.py` (handlerFile: Is the file responsible for the messages coming from the specific board) (eg `python3 ./COMMSmessageHandler.py` for the COMMS board)
