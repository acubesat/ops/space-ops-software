const express = require("express");
const { exec, execSync } = require("child_process");
const cors = require("cors");
const path = require("path");
const app = express();

app.use(
  cors({
    origin: "http://localhost:4200",
  })
);

// app.get("/ls", (req, res) => {
//   exec("find ~ -type d -name  asdasda", (error, stdout, stderr) => {
//     if (stdout == "") {
//       console.log(`hiiii`);
//     }
//     res.send({ output: stdout });
//   });
// });

const homeDirectory = execSync("echo ~").toString().trim();
const yamcsPath = execSync("find ~ -type d -name  yamcs-instance")
  .toString()
  .trim();
const frontendPath = execSync("find ~ -type d -name  frontend-software")
  .toString()
  .trim();

process.chdir(yamcsPath);
exec("mvn yamcs:run", (err, stdout, stderr) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log(stdout);
});

exec("python3 simulator.py", (err, stdout, stderr) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log(stdout);
});

process.chdir(frontendPath);

exec("ng serve --open", (err, stdout, stderr) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log(stdout);
});

app.listen(3000, () => {
  console.log("Server is listening on port 3000");
});

/** testing exec commands    */

// exec("git checkout real-time-graphs", (err, stdout, stderr) => {
//     if (err) {
//       console.error(err);
//       return;
//     }
//     console.log(stdout);
//   });

// app.get("/test", (req, res) => {
//   exec("code . ~/yamcs-instance", (error, stdout, stderr) => {
//     if (error) {
//       console.error(`exec error: ${error}`);
//       return res.status(500).send({ error });
//     }
//     res.send({ output: stdout });
//   });
// });
